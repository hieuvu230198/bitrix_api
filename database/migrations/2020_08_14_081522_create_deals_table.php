<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('DEAL_ID')->nullable();
            $table->string('TITLE')->nullable();
            $table->string('USERNAME')->nullable();
            $table->double('OPPORTUNITY', 15, 2)->nullable();
            $table->double('TAX_VALUE', 15, 2)->nullable();
            $table->unsignedInteger('ASSIGNED_BY_ID')->nullable();
            $table->unsignedInteger('LEAD_ID')->nullable();
            $table->unsignedInteger('COMPANY_ID')->nullable();
            $table->unsignedInteger('CONTACT_ID')->nullable();
            $table->string('CURRENCY')->nullable();
            $table->text('COMPANY_NAME')->nullable();
            $table->unsignedInteger('UF_CRM_1591843618')->nullable();
            $table->unsignedInteger('UF_CRM_1591929672')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal');
    }
}
