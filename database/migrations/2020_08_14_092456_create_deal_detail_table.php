<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal-detail', function (Blueprint $table) {
            $table->bigIncrements('ID_DEAL_DETAIL');
            $table->unsignedInteger('DEAL_ID')->nullable();
            $table->unsignedInteger('ID')->nullable();
            $table->unsignedInteger('PRODUCT_ID')->nullable();
            $table->longText('PRODUCT_DESCRIPTION')->nullable();
            $table->string('MEASURE_NAME')->nullable();
            $table->longText('PRODUCT_NAME')->nullable();
            $table->double('PRICE', 15, 2)->nullable()->default(0);
            $table->double('PRICE_SELL', 15, 2)->nullable()->default(0);
            $table->double('PRICE_PURCHASE', 15, 2)->nullable()->default(0);
            $table->double('PRICE_SUGGEST_CUSTOMER', 15, 2)->nullable()->default(0);
            $table->unsignedInteger('DISCOUNT_PRICE')->nullable()->default(0);
            $table->unsignedInteger('DISCOUNT_PRICE_RATE')->nullable()->default(0);
            $table->double('PRICE_DIFFERENCE', 15, 2)->nullable()->default(0);
            $table->double('PRICE_CUSTOMER_AMOUNT', 15, 2)->nullable()->default(0);
            $table->double('COST_DIFFERENCE', 15, 2)->nullable()->default(0);
            $table->double('COST_OF_HANDLE_DIFFERENCE', 15, 2)->nullable()->default(0);
            $table->unsignedInteger('RATE_DIFFERENCE')->nullable()->default(0);
            $table->double('PURCHASE_PRICE_NOT_TAX', 15, 2)->nullable()->default(0);
            $table->double('SUM_PURCHASE_PRICE_NOT_TAX', 15, 2)->nullable()->default(0);
            $table->double('SUM_PRICE_SELL_TO_CUSTOMER', 15, 2)->nullable()->default(0);
            $table->unsignedInteger('QUANTITY')->nullable();
            $table->unsignedInteger('TAX_RATE')->nullable()->default(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal-detail');
    }
}
