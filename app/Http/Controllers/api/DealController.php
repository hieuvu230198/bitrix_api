<?php

namespace App\Http\Controllers\api;

use App\Models\Deal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    public function save(Request $request) {
        $dealDetail = $request->dealDetail;
        $arrProduct = $request->products;
        $dealNew = Deal::saveDeal($dealDetail, $arrProduct);
        return response()->apiRet();
    }

    public function getDealDetail($id) {
        $dealDetails = Deal::getDealDetail($id);
        return response()->apiRet($dealDetails);
    }
}
