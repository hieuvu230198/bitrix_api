<?php

namespace App\Http\Controllers\api;

use App\Models\DealDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealDeailController extends Controller
{
    public function save(Request $request) {
        $data = !empty($request->all()) ? $request->all() : [];
        if(count($data) > 0) {
            DealDetail::saveDealDetail($request->all());
        }
        return response()->apiRet();
    }
}
