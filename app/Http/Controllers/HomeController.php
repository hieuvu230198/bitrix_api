<?php

namespace App\Http\Controllers;

use App\Models\Deal;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        $deals = Deal::orderBy('DEAL_ID', 'DESC')->paginate(5);
        return view('pages.list-deal', compact('deals'));
    }
}
