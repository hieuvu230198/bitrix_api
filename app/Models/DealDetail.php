<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealDetail extends Model
{
    protected $table = 'deal-detail';
    protected $primaryKey = 'ID_DEAL_DETAIL';
    protected $guarded = [];
    public function deal() {
        return $this->belongsTo(Deal::class);
    }

    public static function saveDealDetail($arrDealDeail) {
        foreach ($arrDealDeail as $dealDetail) {
            $idDealDetail = $dealDetail['ID_DEAL_DETAIL'];
            $oldDealDetail = DealDetail::find($idDealDetail);
            $quantity = $oldDealDetail->QUANTITY;
            $priceSell = !empty($dealDetail['PRICE']) ? $dealDetail['PRICE'] : $oldDealDetail->PRICE;
            $pricePurchase = !empty($dealDetail['PRICE_PURCHASE']) ? $dealDetail['PRICE_PURCHASE'] : $oldDealDetail->PRICE_PURCHASE;
            $priceSuggestCustomer = !empty($dealDetail['PRICE_SUGGEST_CUSTOMER']) ? $dealDetail['PRICE_SUGGEST_CUSTOMER'] : $oldDealDetail->PRICE_SUGGEST_CUSTOMER;;
            $rateDifference = !empty($dealDetail['RATE_DIFFERENCE']) ? $dealDetail['RATE_DIFFERENCE'] : $oldDealDetail->RATE_DIFFERENCE;;
            $discountPriceRate = !empty($dealDetail['DISCOUNT_PRICE_RATE']) ? $dealDetail['DISCOUNT_PRICE_RATE'] : $oldDealDetail->DISCOUNT_PRICE_RATE;
            $discountPrice = $priceSuggestCustomer * $discountPriceRate / 100;
            $priceDifference = $priceSell - $priceSuggestCustomer;
            $costOfHandleDifference = $priceDifference * $rateDifference / 100;
            $priceCustomerAmount  = ($priceDifference - $costOfHandleDifference + $discountPrice) * $quantity;
            $oldDealDetail->PRICE = $priceSell;
            $oldDealDetail->PRICE_PURCHASE = $pricePurchase;
            $oldDealDetail->RATE_DIFFERENCE = $rateDifference;
            $oldDealDetail->PRICE_SUGGEST_CUSTOMER = $priceSuggestCustomer;
            $oldDealDetail->DISCOUNT_PRICE_RATE = $discountPriceRate;
            $oldDealDetail->PRICE_DIFFERENCE = $priceDifference;
            $oldDealDetail->COST_OF_HANDLE_DIFFERENCE = $costOfHandleDifference;
            $oldDealDetail->DISCOUNT_PRICE = $discountPrice;
            $oldDealDetail->PRICE_CUSTOMER_AMOUNT = $priceCustomerAmount;
            $oldDealDetail->SUM_PRICE_SELL_TO_CUSTOMER = $priceSell * $quantity;
            $oldDealDetail->SUM_PURCHASE_PRICE_NOT_TAX = $pricePurchase * $quantity;
            $oldDealDetail->save();
        }
        return true;
    }
}
