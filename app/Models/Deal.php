<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $table = 'deal';
    protected $primaryKey='id';

    public function dealDetails() {
        return $this->hasMany(DealDetail::class);
    }

    public static function saveDeal($dealDetail, $arrProduct) {
        $dealNew = Deal::where('DEAL_ID', $dealDetail['ID'])->first();
        if (!$dealNew) {
            $dealNew = new Deal();
        }
        $dealNew->DEAL_ID = $dealDetail['ID'];
        $dealNew->TITLE = $dealDetail['TITLE'];
        $dealNew->OPPORTUNITY = $dealDetail['OPPORTUNITY'];
        $dealNew->ASSIGNED_BY_ID = $dealDetail['ASSIGNED_BY_ID'];
        $dealNew->TAX_VALUE = $dealDetail['TAX_VALUE'];
        $dealNew->CURRENCY = $dealDetail['CURRENCY'];
        $dealNew->USERNAME = $dealDetail['USERNAME'];
        $dealNew->COMPANY_NAME = $dealDetail['COMPANY_NAME'];
        $dealNew->UF_CRM_1591929672 = $dealDetail['UF_CRM_1591929672'];
        $dealNew->UF_CRM_1591843618 = $dealDetail['UF_CRM_1591843618'];
        $dealNew->save();
        $dealNew->dealDetails()->delete();
        self::saveDealDetail($dealNew, $arrProduct);
        return true;
    }
    public static function getDealDetail($id) {
        $deal = Deal::findOrFail($id);
        return $deal->dealDetails;
    }

    public static function saveDealDetail($dealNew, $arrProduct) {
        foreach ($arrProduct as $product) {
            $dealDetailNew = new DealDetail;
            $dealDetailNew->DEAL_ID = $dealNew->id;
            $dealDetailNew->PRODUCT_ID = $product['PRODUCT_ID'];
            $dealDetailNew->PRODUCT_NAME = $product['PRODUCT_NAME'];
            $dealDetailNew->PRICE = $product['PRICE'];
            $dealDetailNew->QUANTITY = $product['QUANTITY'];
            $dealDetailNew->MEASURE_NAME = $product['MEASURE_NAME'];
            $dealDetailNew->TAX_RATE = $product['TAX_RATE'];
            $dealDetailNew->save();
        }
    }
}
