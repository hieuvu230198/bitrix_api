## Field custom
    - Giá mua: PRICE_PURCHASE
    - Đơn giá bán: PRICE [DONE]
    - Đơn giá yêu cầu: PRICE_SUGGEST_CUSTOMER [DONE]
    - % xử lý chênh: RATE_DIFFERENCE     [DONE]
    - Tiền chênh: PRICE_DIFFERENCE [Giá bán - Giá khách yêu cầu]  [DONE]
    - Phí xử lý chênh: COST_OF_HANDLE_DIFFERENCE [ tiền chênh * % xử lý chênh]  [DONE]
    - Chiết khấu: DISCOUNT_PRICE [đơn giá yêu cầu * % chiết khấu]
    - % Chiết khấu: DISCOUNT_PRICE_RATE [DONE]
    - Tiền khách hàng nhận: PRICE_CUSTOMER_AMOUNT[tiền chênh - phí xử lý chênh + chiết khấu] * số lượng
    - Thành tiền: SUM_PRICE_SELL_TO_CUSTOMER [đơn giá bán * số lượng]
    - Giá mua tổng cộng chưa VAT: SUM_PURCHASE_PRICE_NOT_TAX [tổng mua]
    - Giá mua thuế VAT: [Tổng cộng (chưa bao gồm thuế) * 0.1]
    - Tổng cộng (Đã bao gồm thuế): [giá mua tổng cộng chưa VAT + giá mua thuế VAT]
    - Giá bán tổng cộng (chưa bao gồm thuế)	: [Thành tiền += Thành tiền]
    - Giá bán thuế VAT	: [Giá bán tổng cộng (chưa bao gồm thuế) * 0.1]
    - Giá bán Tổng cộng (Đã bao gồm thuế): [Giá bán tổng cộng (chưa bao gồm thuế) + Giá bán thuế VAT]



