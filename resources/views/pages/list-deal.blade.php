@extends('layouts.layout')

@section('content')
    <div class="container mt-xl-15 mt-sm-15 mt-15">
{{--        <!-- Title -->--}}
{{--        <div class="hk-pg-header">--}}
{{--            <div>--}}
{{--                <h2 class="hk-pg-title font-weight-600">Project Management</h2>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- /Title -->--}}
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12" id="app">
                <list-deal-component :deals='@json($deals)'></list-deal-component>
            </div>
            <div class="col-xl-12">
                <div class="dataTables_paginate">
                    {{ $deals->onEachSide(5)->links() }}
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection
@push('javascript')
    <script src="{{ asset('js/app.js') }}"></script>
@endpush
