<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Dashboard</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('assets/vendors/morris.js/morris.css') }}" rel="stylesheet" type="text/css" />
    <!-- Toggles CSS -->
    <link href="{{ asset('assets/vendors/jquery-toggles/css/toggles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/jquery-toggles/css/themes/toggles-light.css') }}" rel="stylesheet" type="text/css">
    <!-- Toastr CSS -->
    <link href="{{ asset('assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/dist/css/style.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="loader-pendulums"></div>
</div>
<!-- /Preloader -->

<!-- HK Wrapper -->
<div class="hk-wrapper hk-horizontal-nav">

    <!-- Top Navbar -->
    <nav class="navbar navbar-expand-xl navbar-dark fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
        <a class="navbar-brand" href="#">
           Bluecom
        </a>
        <ul class="navbar-nav hk-navbar-content">
            <li class="nav-item dropdown dropdown-authentication">
                <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media">
                        <div class="media-img-wrap">
                            <div class="avatar">
                                <img src="{{ asset('assets/dist/img/avatar12.jpg') }}" alt="user" class="avatar-img rounded-circle">
                            </div>
                            <span class="badge badge-success badge-indicator"></span>
                        </div>
                        <div class="media-body">
                            <span>Hieu Vu<i class="zmdi zmdi-chevron-down"></i></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-account"></i><span>Profile</span></a>
                    <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-settings"></i><span>Settings</span></a>
                    <div class="dropdown-divider"></div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-power"></i><span>Log out</span></a>
                </div>
            </li>
        </ul>
    </nav>
    <form role="search" class="navbar-search">
        <div class="position-relative">
            <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i data-feather="search"></i></span></a>
            <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
            <a id="navbar_search_close" class="navbar-search-close" href="#"><span class="feather-icon"><i data-feather="x"></i></span></a>
        </div>
    </form>
    <!-- /Top Navbar -->

    <!--Horizontal Nav-->
    <nav class="hk-nav hk-nav-light">
        <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
        <div class="nicescroll-bar">
            <div class="navbar-nav-wrap">
                <ul class="navbar-nav flex-row">
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);">
                            <span class="feather-icon"><i data-feather="activity"></i></span>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-with-indicator" href="javascript:void(0);" data-toggle="collapse" data-target="#app_drp">
                            <span class="feather-icon"><span class="badge badge-primary badge-indicator badge-indicator-sm badge-pill"></span><i data-feather="package"></i></span>
                            <span class="nav-link-text">Application</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#pages_drp">
                            <span class="feather-icon"><i data-feather="file-text"></i></span>
                            <span class="nav-link-text">Pages</span>
                        </a>
                        <ul id="pages_drp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#auth_drp">
                                            Authentication
                                        </a>
                                        <ul id="auth_drp" class="nav flex-column collapse collapse-level-2">
                                            <li class="nav-item">
                                                <ul class="nav flex-column">
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#signup_drp">
                                                            Sign Up
                                                        </a>
                                                        <ul id="signup_drp" class="nav flex-column collapse collapse-level-2">
                                                            <li class="nav-item">
                                                                <ul class="nav flex-column">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="signup.html">Cover</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="signup-simple.html">Simple</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#signin_drp">
                                                            Login
                                                        </a>
                                                        <ul id="signin_drp" class="nav flex-column collapse collapse-level-2">
                                                            <li class="nav-item">
                                                                <ul class="nav flex-column">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="login.html">Cover</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="login-simple.html">Simple</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#recover_drp">
                                                            Recover Pwd
                                                        </a>
                                                        <ul id="recover_drp" class="nav flex-column collapse collapse-level-2">
                                                            <li class="nav-item">
                                                                <ul class="nav flex-column">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="forgot-password.html">Forgot Password</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="reset-password.html">Reset Password</a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="lock-screen.html">Lock Screen</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="404.html">Error 404</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="maintenance.html">Maintenance</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>

                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="profile.html">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="invoice.html">Invoice</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="gallery.html">Gallery</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="activity.html">Activity</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="faq.html">Faq</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="#">
                            <span class="feather-icon"><i data-feather="eye"></i></span>
                            <span class="nav-link-text">Changelog</span>
                            <span class="badge badge-success badge-sm badge-pill">v 1.0</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
    <!--/Horizontal Nav-->

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Container -->
        <div class="container mt-xl-50 mt-sm-30 mt-15">
            <!-- Title -->
            <div class="hk-pg-header">
                <div>
                    <h2 class="hk-pg-title font-weight-600">Project Management</h2>
                </div>
            </div>
            <!-- /Title -->
            <!-- Row -->
            <div class="row">
                <div class="col-xl-12">
                    <section class="hk-sec-wrapper">
{{--                        <h5 class="hk-sec-title">Large table</h5>--}}
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="datable_1_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Search" aria-controls="datable_1"></label></div>
                            </div>
                            <div class="col-sm">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-lg mb-0 table-primary">
                                            <thead class="thead-primary">
                                            <tr>
                                                <th>Sản Phẩm</th>
                                                <th>Đơn Vị</th>
                                                <th>Giá Mua</th>
                                                <th>Nhà Cung Cấp</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Lunar probe project</td>
                                                <td>May 15, 2015</td>
                                                <td>100,000</td>
                                                <td>Công ty Cổ phần Đầu tư và Phát triển Quốc Gia ADG</td>
                                            </tr>
                                            <tr>
                                                <td>Dream successful plan</td>
                                                <td>July 1, 2015</td>
                                                <td>100,000</td>
                                                <td>Công ty Cổ phần Đầu tư và Phát triển Quốc Gia ADG</td>
                                            </tr>
                                            <tr>
                                                <td>Office automatization</td>
                                                <td>Apr 12, 2015</td>
                                                <td>100,000</td>
                                                <td>Công ty Cổ phần Đầu tư và Phát triển Quốc Gia ADG</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-12">
                    <div class="dataTables_paginate">
                        <ul class="pagination">
                            <li class="paginate_button page-item previous" id="datable_1_previous"><a href="#" aria-controls="datable_1" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                            <li class="paginate_button page-item active"><a href="#" class="page-link">1</a></li>
                            <li class="paginate_button page-item "><a href="#" class="page-link">2</a></li>
                            <li class="paginate_button page-item "><a href="#" class="page-link">3</a></li>
                            <li class="paginate_button page-item "><a href="#" class="page-link">4</a></li>
                            <li class="paginate_button page-item "><a href="#" class="page-link">5</a></li>
                            <li class="paginate_button page-item "><a href="#" class="page-link">6</a></li>
                            <li class="paginate_button page-item next" id="datable_1_next"><a href="#" aria-controls="datable_1" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->

        <!-- Footer -->
        <div class="hk-footer-wrap container">
            <footer class="footer">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">HieuVu</a> © 2020</p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p class="d-inline-block">Follow us</p>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /Footer -->
    </div>
    <!-- /Main Content -->

</div>
<!-- /HK Wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ asset('assets/dist/js/jquery.slimscroll.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ asset('assets/dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{ asset('assets/dist/js/feather.min.js') }}"></script>

<!-- Toggles JavaScript -->
<script src="{{ asset('assets/vendors/jquery-toggles/toggles.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/toggle-data.js') }}"></script>
<!-- Bootstrap-table JavaScript -->
<script src="{{ asset('assets/vendors/bootstrap-table/dist/bootstrap-table.min.js') }}"></script>
<!-- Init JavaScript -->
<script src="{{ asset('assets/dist/js/init.js') }}"></script>

</body>

</html>
