export default {
    props: ['deals'],
    data() {
        return {
            API_URL: process.env.MIX_APP_URL,
            arrDeal: this.deals.data,
            arrDealDetail: [],
            arrPriceSellCustomer: [],
            fieldCustom: {
                PRICE: 'PRICE',
                PRICE_PURCHASE: 'PRICE_PURCHASE',
                PRICE_SUGGEST_CUSTOMER: 'PRICE_SUGGEST_CUSTOMER',
                RATE_DIFFERENCE: 'RATE_DIFFERENCE',
                DISCOUNT_PRICE_RATE: 'DISCOUNT_PRICE_RATE',
            },
            arrNewDealDetail: [],
            dealID: 0,
            isDataChange: false
        }
    },
    methods: {
        showDealDetail(dealId) {
            $('#modal-deal-detail').modal('show');
            this.dealID = dealId;
            axios.get(this.API_URL+'/api/deal/'+dealId+'/detail').then(response => {
                this.arrDealDetail = response.data.data;
            });
        },
        saveDealDetail() {
            let data = [];
            if(this.isDataChange) {
                data = this.arrNewDealDetail;
            } else {
                data = this.arrDealDetail.map(item => {
                    return {
                        ID_DEAL_DETAIL: item.ID_DEAL_DETAIL,
                        PRICE: item.PRICE,
                        PRICE_PURCHASE: item.PRICE_PURCHASE,
                        PRICE_SUGGEST_CUSTOMER: item.PRICE_SUGGEST_CUSTOMER,
                        RATE_DIFFERENCE: item.RATE_DIFFERENCE,
                        DISCOUNT_PRICE_RATE: item.DISCOUNT_PRICE_RATE,
                    }
                });
            }
            axios.post(this.API_URL+'/api/deal-detail/save', data).then(response => {
                this.showDealDetail(this.dealID);
            });
        },
        addValueToFieldCustom(event, dealDetailId, fieldCustom) {
            this.isDataChange = true;
            if(!this.preventEventCheckNumberInput(event)) {
                return false;
            }
            let value = event.target.value.toString().replace(/,/g, '');
            let itemOld = this.arrNewDealDetail.find(item => item.ID_DEAL_DETAIL === dealDetailId);
            if (!itemOld) {
                this.arrNewDealDetail.push({ID_DEAL_DETAIL: dealDetailId, [fieldCustom]: value});
            } else {
                itemOld[fieldCustom] = value;
            }
            event.target.value = this.formatCurrency(value);
        },
        formatCurrency(value) {
            let realValue = value.toString().replace(/,/g, '');
            const splitStr = ',';
            if (realValue === null || realValue === undefined) {
                return '';
            }
            return realValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, splitStr);
        },
        preventEventCheckNumberInput(event) {
            let regex = /^\d*$/;
            let value = event.target.value.toString().replace(/,/g, '');
            if(!regex.test(value)) {
                event.target.value = '';
                return false;
            }
            return true;
        }
    },
    computed: {
        computedArrDealDetail() {
            return this.arrDealDetail;
        },
        SUM_PURCHASE_PRICE_NOT_TAX() {
            let arrPrice = this.arrDealDetail.map(item => {
                return item.SUM_PURCHASE_PRICE_NOT_TAX;
            });
            return arrPrice.reduce((a, b) => a + b, 0);
        },
        SUM_PURCHASE_PRICE_INCLUDE_TAX() {
            let arrPrice = this.arrDealDetail.map(item => {
                let newItem = item.SUM_PURCHASE_PRICE_NOT_TAX;
                return newItem * 0.1;
            });
            return arrPrice.reduce((a, b) => a + b, 0);
        },
        ALL_SUM_PURCHASE_PRICE_INCLUDE_TAX() {
            return this.SUM_PURCHASE_PRICE_INCLUDE_TAX + this.SUM_PURCHASE_PRICE_NOT_TAX;
        },
        SUM_SELL_CUSTOMER_PRICE_NOT_TAX() {
            let arrPrice = this.arrDealDetail.map(item => {
                return item.SUM_PRICE_SELL_TO_CUSTOMER;
            });
            return arrPrice.reduce((a, b) => a + b, 0);
        },
        SUM_SELL_CUSTOMER_PRICE_INCLUDE_TAX() {
            let arrPrice = this.arrDealDetail.map(item => {
                let newItem = item.SUM_PRICE_SELL_TO_CUSTOMER;
                return newItem * 0.1;
            });
            return arrPrice.reduce((a, b) => a + b, 0);
        },
        ALL_SUM_SELL_CUSTOMER_PRICE_INCLUDE_TAX() {
            return this.SUM_PURCHASE_PRICE_INCLUDE_TAX + this.SUM_SELL_CUSTOMER_PRICE_NOT_TAX;
        },
    }
}
