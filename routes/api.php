<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return response()->apiRet("OKE");
});
Route::get('/deal/{id}/detail', 'api\DealController@getDealDetail');
Route::post('/deal/save', 'api\DealController@save');
Route::post('/deal-detail/save', 'api\DealDeailController@save');
